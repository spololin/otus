#расшифровка архива
openssl des3 -salt -k "password" -d -in backup.xbstream.gz.des3 -out backup.xbstream.gz
gzip -d backup.xbstream.gz

#распаковка архива
xbstream -x < backup.xbstream

#подготовка для восстановления
xtrabackup --prepare --export --target-dir=/path/to/partial/backup

#создаем новую таблицу из дампа
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`,`body`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


#отключаем tablespace
ALTER TABLE articles DISCARD TABLESPACE

#копируем файл ibd из распакованного архива
copy...

#Импортируем данные из бэкапа у нужную таблицу
ALTER TABLE articles IMPORT TABLESPACE

