-- создание табличных пространств
CREATE TABLESPACE customers OWNER postgres LOCATION 'c:\Program Files\PostgreSQL\11\data\otus_customers';
CREATE TABLESPACE sales OWNER postgres LOCATION 'c:\Program Files\PostgreSQL\11\data\otus_sales';
CREATE TABLESPACE products OWNER postgres LOCATION 'c:\Program Files\PostgreSQL\11\data\otus_products';

--создание БД
CREATE DATABASE otus_rdbms;

--создание схем
CREATE SCHEMA customers AUTHORIZATION postgres;
CREATE SCHEMA products AUTHORIZATION postgres;
CREATE SCHEMA sales AUTHORIZATION postgres;

--создание ролей
CREATE USER person with LOGIN PASSWORD '1234';
CREATE USER manager with LOGIN PASSWORD '12345';

--создание таблиц
CREATE TABLE customers.correspondence_languages (
	code varchar(5) NOT NULL,
	description varchar(50) NULL,
	id serial NOT NULL,
	CONSTRAINT correspondence_languages_pk PRIMARY KEY (id),
	CONSTRAINT correspondence_languages_un UNIQUE (code)
) TABLESPACE customers;

CREATE TABLE customers.titles (
	id serial NOT NULL,
	"name" varchar(20) NOT NULL,
	CONSTRAINT titles_pk PRIMARY KEY (id),
	CONSTRAINT titles_un2 UNIQUE (name)
) TABLESPACE customers;

CREATE TABLE customers.marital_statuses (
	id serial NOT NULL,
	"name" varchar(50) NOT NULL,
	CONSTRAINT marital_statuses_pk PRIMARY KEY (id),
	CONSTRAINT marital_statuses_un UNIQUE (name)
) TABLESPACE customers;

CREATE TABLE customers.countries (
	id serial NOT NULL,
	"name" varchar(200) NULL,
	code varchar(5) NOT NULL,
	CONSTRAINT countries_pk PRIMARY KEY (id),
	CONSTRAINT countries_un UNIQUE (code)
) TABLESPACE customers;

CREATE TABLE customers.regions (
	id int8 NOT NULL,
	"name" varchar(200) NOT NULL,
	id_country int4 NOT NULL,
	CONSTRAINT regions_pk PRIMARY KEY (id),
	CONSTRAINT regions_un UNIQUE (id_country, name)
) TABLESPACE customers;
ALTER TABLE customers.regions ADD CONSTRAINT regions_fk FOREIGN KEY (id_country) REFERENCES customers.countries(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE customers.cities (
	id serial NOT NULL,
	"name" varchar(200) NOT NULL,
	id_region int4 NOT NULL,
	CONSTRAINT cities_pk PRIMARY KEY (id),
	CONSTRAINT cities_un UNIQUE (id_region, name)
) TABLESPACE customers;
ALTER TABLE customers.cities ADD CONSTRAINT cities_fk FOREIGN KEY (id_region) REFERENCES customers.regions(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE customers.streets (
	id serial NOT NULL,
	"name" varchar(200) NOT NULL,
	id_city int4 NOT NULL,
	CONSTRAINT streets_pk PRIMARY KEY (id),
	CONSTRAINT streets_un UNIQUE (name, id_city)
) TABLESPACE customers;
ALTER TABLE customers.streets ADD CONSTRAINT streets_fk FOREIGN KEY (id_city) REFERENCES customers.cities(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE customers.building_numbers (
	id serial NOT NULL,
	number_house varchar(10) NOT NULL,
	id_street int4 NOT NULL,
	postal_code varchar(15) NULL,
	number_room varchar(3) NULL,
	CONSTRAINT building_numbers_pk PRIMARY KEY (id)
) TABLESPACE customers;
ALTER TABLE customers.building_numbers ADD CONSTRAINT building_numbers_fk FOREIGN KEY (id_street) REFERENCES customers.streets(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE customers.customers_building_numbers (
	id serial NOT NULL,
	customer_id int4 NOT NULL,
	building_number_id int4 NOT NULL,
	CONSTRAINT customers_building_numbers_pk PRIMARY KEY (id),
	CONSTRAINT customers_building_numbers_un UNIQUE (customer_id, building_number_id)
) TABLESPACE customers;
ALTER TABLE customers.customers_building_numbers ADD CONSTRAINT customers_building_numbers_fk FOREIGN KEY (building_number_id) REFERENCES customers.building_numbers(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE customers.customers (
	id serial NOT NULL,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	middle_name varchar(50) NULL,
	birth_date date NOT NULL,
	id_title int4 NULL,
	id_bulding_number int4 NULL,
	id_correspondence_language int4 NULL,
	gender int2 NULL,
	id_material_status int4 NULL,
	id_status int4 NULL,
	phone varchar(16) NULL,
	email varchar(50) NULL,
	bonus int4 NULL,
	CONSTRAINT customers_check CHECK ((gender = ANY (ARRAY[0, 1]))),
	CONSTRAINT customers_check2 CHECK ((birth_date > '1900-01-01'::date)),
	CONSTRAINT customers_pk PRIMARY KEY (id),
	CONSTRAINT customers_un UNIQUE (phone),
	CONSTRAINT customers_un2 UNIQUE (email)
) TABLESPACE customers;
ALTER TABLE customers.customers ADD CONSTRAINT customers_fk FOREIGN KEY (id_title) REFERENCES customers.titles(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE customers.customers ADD CONSTRAINT customers_fk_1 FOREIGN KEY (id_correspondence_language) REFERENCES customers.correspondence_languages(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE customers.customers ADD CONSTRAINT customers_fk_2 FOREIGN KEY (id_material_status) REFERENCES customers.marital_statuses(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE customers.customers ADD CONSTRAINT customers_fk_3 FOREIGN KEY (id_bulding_number) REFERENCES customers.building_numbers(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE customers.customers ADD CONSTRAINT customers_fk_4 FOREIGN KEY (id_status) REFERENCES customers.customer_statuses(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE customers.customer_statuses (
	id serial NOT NULL,
	"name" varchar(50) NOT NULL,
	percent_sale int2 NOT NULL,
	CONSTRAINT customer_statuses_pk PRIMARY KEY (id),
	CONSTRAINT customer_statuses_un UNIQUE (name),
	CONSTRAINT customer_statuses_un2 UNIQUE (percent_sale)
) TABLESPACE customers;

CREATE TABLE products.category_products (
	id serial NOT NULL,
	"name" varchar(200) NOT NULL,
	characteristic json NULL,
	CONSTRAINT category_products_pk PRIMARY KEY (id)
) TABLESPACE products;

CREATE TABLE sales.organizations (
	id serial NOT NULL,
	"name" varchar(100) NOT NULL,
	fullname varchar(200) NULL,
	inn varchar(13) NOT NULL,
	kpp varchar(13) NOT NULL,
	id_building_number int4 NOT NULL,
	type_organisation int2 NULL,
	CONSTRAINT organizations_check CHECK ((type_organisation = ANY (ARRAY[0, 1]))),
	CONSTRAINT organizations_pk PRIMARY KEY (id)
) TABLESPACE sales;
ALTER TABLE sales.organizations ADD CONSTRAINT organizations_fk FOREIGN KEY (id_building_number) REFERENCES customers.building_numbers(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE products.products (
	id serial NOT NULL,
	"name" varchar(200) NOT NULL,
	id_category_product int4 NULL,
	id_manufacturer int4 NULL,
	id_provider int4 NULL,
	CONSTRAINT products_pk PRIMARY KEY (id)
) TABLESPACE products;
ALTER TABLE products.products ADD CONSTRAINT products_fk FOREIGN KEY (id_category_product) REFERENCES products.category_products(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE products.products ADD CONSTRAINT products_fk_1 FOREIGN KEY (id_manufacturer) REFERENCES sales.organizations(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE products.products ADD CONSTRAINT products_fk_2 FOREIGN KEY (id_provider) REFERENCES sales.organizations(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE products.history_prices (
	id serial NOT NULL,
	product_id int4 NOT NULL,
	date_from date NOT NULL,
	date_to date NULL,
	"cost" numeric(10,2) NOT NULL,
	CONSTRAINT history_prices_check CHECK ((date_from <= date_to)),
	CONSTRAINT history_prices_pk PRIMARY KEY (id)
)TABLESPACE products;
ALTER TABLE products.history_prices ADD CONSTRAINT history_prices_fk FOREIGN KEY (product_id) REFERENCES products.products(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE products.prices (
	id serial NOT NULL,
	product_id int4 NULL,
	"cost" numeric(10,2) NOT NULL,
	CONSTRAINT prices_check CHECK ((cost > (0)::double precision)),
	CONSTRAINT prices_pk PRIMARY KEY (id),
	CONSTRAINT prices_un UNIQUE (product_id, cost)
) TABLESPACE products;
ALTER TABLE products.prices ADD CONSTRAINT prices_fk FOREIGN KEY (product_id) REFERENCES products.products(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE sales.type_payments (
	id serial NOT NULL,
	type_name varchar(50) NOT NULL,
	CONSTRAINT type_payments_pk PRIMARY KEY (id)
) TABLESPACE sales;

CREATE TABLE sales.orders (
	id serial NOT NULL,
	date_order date NOT NULL,
	date_receiving date NULL,
	id_type_payment int4 NOT NULL,
	id_customer int4 NOT NULL,
	"cost" numeric(10,2) NOT NULL,
	bonus int4 NULL DEFAULT 0,
	CONSTRAINT orders_pk PRIMARY KEY (id)
) TABLESPACE sales;
ALTER TABLE sales.orders ADD CONSTRAINT orders_fk FOREIGN KEY (id_type_payment) REFERENCES sales.type_payments(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE sales.orders ADD CONSTRAINT orders_fk_1 FOREIGN KEY (id_customer) REFERENCES customers.customers(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE sales.purchases (
	id serial NOT NULL,
	id_product int4 NOT NULL,
	count int4 NOT NULL DEFAULT 1,
	id_order int4 NULL,
	bonus int4 NULL,
	CONSTRAINT purchases_check CHECK ((count > 0)),
	CONSTRAINT purchases_pk PRIMARY KEY (id)
) TABLESPACE sales;
ALTER TABLE sales.purchases ADD CONSTRAINT purchases_fk FOREIGN KEY (id_order) REFERENCES sales.orders(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE sales.purchases ADD CONSTRAINT purchases_fk_1 FOREIGN KEY (id_product) REFERENCES products.products(id) ON UPDATE CASCADE ON DELETE SET NULL;

CREATE TABLE sales.order_statuses (
	id serial NOT NULL,
	"name" varchar(50) NOT NULL,
	CONSTRAINT order_statuses_pk PRIMARY KEY (id),
	CONSTRAINT order_statuses_un UNIQUE (name)
) TABLESPACE sales;

CREATE TABLE sales.history_order_statuses (
	id serial NOT NULL,
	date_status date NULL,
	id_order int4 NOT NULL,
	id_status int4 NOT NULL,
	CONSTRAINT history_order_statuses_pk PRIMARY KEY (id),
	CONSTRAINT history_order_statuses_un UNIQUE (date_status, id_order, id_status)
) TABLESPACE sales;
ALTER TABLE sales.history_order_statuses ADD CONSTRAINT history_order_statuses_fk FOREIGN KEY (id_order) REFERENCES sales.orders(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE sales.history_order_statuses ADD CONSTRAINT history_order_statuses_fk_1 FOREIGN KEY (id_status) REFERENCES sales.order_statuses(id) ON UPDATE CASCADE ON DELETE SET NULL;
