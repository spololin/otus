#Процедура получения товаров.
DELIMITER //  
CREATE PROCEDURE `select_products` (IN cat varchar(100), IN name varchar(50), IN cost decimal(10,2), IN org varchar(150), IN f_order varchar(50), in f_limit int, in f_offset int)  
BEGIN 
	DECLARE str VARCHAR(15000);
    declare condition_and int(1);
    set condition_and = 0;
    set @str = 'select prd.* from products as prd 
    left join category_products as cat on cat.id=prd.id_category_product 
    left join prices as prs on prs.product_id=prd.id
    left join organizations as org on org.id=prd.id_manufacturer';
    IF (cat is not null or name is not null or cost is not null or org is not null) then 
		set @str = concat(@str, ' where ');
    end if;
    IF cat is not null then 
		set @str = concat(@str, ' cat.name like concat(''%'', ''', cat, ''', ''%'')');
        set condition_and = 1;
    end if;
    IF name is not null then 
		IF condition_and=1 then
			set @str = concat(@str, ' and ');
		end if;
		set @str = concat(@str, ' prd.name like concat(''%'', ''', name, ''', ''%'')');
        set condition_and = 1;
    end if;
    IF cost is not null then 
		IF condition_and=1 then
			set @str = concat(@str, ' and ');
		end if;
		set @str = concat(@str,  concat(' prs.cost=', cost));
        set condition_and = 1;
    end if;
    IF org is not null then 
		IF condition_and=1 then
			set @str = concat(@str, ' and ');
		end if;
		set @str = concat(@str, ' org.fullname like concat(''%'', ''', org, ''', ''%'')');
        set condition_and = 1;
    end if;
    IF f_order is not null then 
		set @str = concat(@str,  concat(' order by ', f_order));
	else
		set @str = concat(@str,  ' order by id');
    end if;
    IF f_limit is not null then 
		set @str = concat(@str,  concat(' limit ', f_limit));
        IF f_offset is not null then 
			set @str = concat(@str,  concat(' offset ', f_offset));
		end if;
    end if;

	-- select @str as '1';
	PREPARE getSql FROM @str;
	EXECUTE getSql;
	DEALLOCATE PREPARE getSql;
END //
delimiter ;


#Создание пользователей.
DROP PROCEDURE IF EXISTS otusdb.create_users;
delimiter //
create procedure otusdb.create_users()
BEGIN
	CREATE USER 'client' IDENTIFIED BY '123';
	grant execute ON PROCEDURE otusdb.select_products to 'client';
	FLUSH PRIVILEGES;
    CREATE USER 'manager' IDENTIFIED BY '1234';
	grant execute ON PROCEDURE otusdb.get_orders to 'manager';
	FLUSH PRIVILEGES;
END
//
delimiter ;





