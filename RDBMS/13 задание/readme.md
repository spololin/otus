Полнотекстовый поиск
Для таблиц products и category_products добавил для полей name индексы типа FULLTEXT.
Рабочий запрос:
SELECT p.*
FROM otusdb.products as p
         JOIN otusdb.category_products as c ON p.id_category_product = c.id
WHERE match(p.name) against('red' IN NATURAL LANGUAGE MODE) 
OR match(c.name) against('red' IN NATURAL LANGUAGE MODE);

Индексы
У меня пока что мало данных вставлено и при любой выборке время исполнения запроса всегда составляет 0.00s. В будущем индексы возможно изменятся. 
