SET AUTOCOMMIT=0;

DROP PROCEDURE IF EXISTS otusdb.insert_data_correspondence_languages;
delimiter //
create procedure otusdb.insert_data_correspondence_languages()
BEGIN
	insert into otusdb.correspondence_languages (code, description) values ('RU', 'Русский');
	insert into otusdb.correspondence_languages (code, description) values ('UA', 'Украинский');
	insert into otusdb.correspondence_languages (code, description) values ('BY', 'Белорусский');
	insert into otusdb.correspondence_languages (code, description) values ('GE', 'Немецкий');
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_titles;
delimiter //
create procedure otusdb.insert_data_titles()
BEGIN
	insert into otusdb.titles (name) values ('Mr');
	insert into otusdb.titles (name) values ('Mrs');
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_marital_statuses;
delimiter //
create procedure otusdb.insert_data_marital_statuses()
BEGIN
	insert into otusdb.marital_statuses (name) values ('Замужем');
	insert into otusdb.marital_statuses (name) values ('Женат');
	insert into otusdb.marital_statuses (name) values ('Свобода');
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_customer_statuses;
delimiter //
create procedure otusdb.insert_data_customer_statuses()
BEGIN
	insert into otusdb.customer_statuses (name, percent_sale) values ('Новый клиент', 3);
	insert into otusdb.customer_statuses (name, percent_sale) values ('Постоянный покупатель', 5);
	insert into otusdb.customer_statuses (name, percent_sale) values ('Опт', 10);
	insert into otusdb.customer_statuses (name, percent_sale) values ('Крупный опт', 15);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_countries;
delimiter //
create procedure otusdb.insert_data_countries()
BEGIN
	insert into otusdb.countries (name) values ('Россия');
	insert into otusdb.countries (name) values ('Украина');
	insert into otusdb.countries (name) values ('Белоруссия');
	insert into otusdb.countries (name) values ('Германия');
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_regions;
delimiter //
create procedure otusdb.insert_data_regions()
BEGIN
	insert into otusdb.regions (name, id_country) values ('Воронежская область', 1); 
	insert into otusdb.regions (name, id_country) values ('Липецкая область', 1);
	insert into otusdb.regions (name, id_country) values ('Московская область', 1);
	insert into otusdb.regions (name, id_country) values ('Полтавская вотчина', 2);
	insert into otusdb.regions (name, id_country) values ('Центральная область', 3);
	insert into otusdb.regions (name, id_country) values ('ФРГ', 4);
	insert into otusdb.regions (name, id_country) values ('ГДР', 4);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_cities;
delimiter //
create procedure otusdb.insert_data_cities()
BEGIN
	insert into otusdb.cities (name, id_region) values ('Воронеж', 1);
	insert into otusdb.cities (name, id_region) values ('Лиски', 1);
	insert into otusdb.cities (name, id_region) values ('Задонск', 2);
	insert into otusdb.cities (name, id_region) values ('Москва', 3);
	insert into otusdb.cities (name, id_region) values ('Киев', 3);
	insert into otusdb.cities (name, id_region) values ('Минск', 4);
	insert into otusdb.cities (name, id_region) values ('Берлин', 5);
	insert into otusdb.cities (name, id_region) values ('Висмар', 6);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_streets;
delimiter //
create procedure otusdb.insert_data_streets()
BEGIN
	insert into otusdb.streets (name, id_city) values ('ул. Артамонова', 1);
	insert into otusdb.streets (name, id_city) values ('ул. Брусилова', 3);
	insert into otusdb.streets (name, id_city) values ('ул. Преображенская', 4);
	insert into otusdb.streets (name, id_city) values ('пл. Ордена Славы', 5);
	insert into otusdb.streets (name, id_city) values ('пр. Победы', 6);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_building_numbers;
delimiter //
create procedure otusdb.insert_data_building_numbers()
BEGIN
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('34В', 1, '394000', 60);
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('38', 1, '394000', 100);
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('11', 2, '398000', 7);
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('1', 3, '487890', 11);
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('17', 4, '114A', 14);
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('3', 5, '89990', 11);
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('9', 1, '89990', null);
	insert into otusdb.building_numbers (number_house, id_street, postal_code, number_room) values ('8', 5, '89990', 1);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_customers;
delimiter //
create procedure otusdb.insert_data_customers()
BEGIN
	insert into otusdb.customers (first_name, last_name, birth_date, id_title, id_bulding_number, id_correspondence_language, gender, id_material_status, id_status, phone, email, bonus) values ('Имя1', 'Фамилия1', '1988-01-01', 1, 1, 1, 1, 2, 2, '+79001112233', '123@mail.ru', 321);
	insert into otusdb.customers (first_name, last_name, birth_date, id_title, id_bulding_number, id_correspondence_language, gender, id_material_status, id_status, phone, email, bonus) values ('Имя2', 'Фамилия2', '1988-02-02', 1, 2, 2, 1, 3, 1, '+79001112244', '1231@mail.ru', 51);
	insert into otusdb.customers (first_name, last_name, birth_date, id_title, id_bulding_number, id_correspondence_language, gender, id_material_status, id_status, phone, email, bonus) values ('Имя3', 'Фамилия3', '1988-03-03', 2, 3, 3, 0, 1, 3, '+79001112256', '1232@mail.ru', 3210);
	insert into otusdb.customers (first_name, last_name, birth_date, id_title, id_bulding_number, id_correspondence_language, gender, id_material_status, id_status, phone, email, bonus) values ('Имя4', 'Фамилия4', '1988-04-04', 2, 4, 4, 0, 3, 3, '+79001112277', '1233@mail.ru', 3211);
	insert into otusdb.customers (first_name, last_name, birth_date, id_title, id_bulding_number, id_correspondence_language, gender, id_material_status, id_status, phone, email, bonus) values ('Имя5', 'Фамилия5', '1988-05-05', 1, 5, 1, 1, 2, 4, '+79001112288', '1234@mail.ru', 32100);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_customers_building_numbers;
delimiter //
create procedure otusdb.insert_data_customers_building_numbers()
BEGIN
	insert into otusdb.customers_building_numbers (customer_id, building_number_id) values (1, 1);
	insert into otusdb.customers_building_numbers (customer_id, building_number_id) values (1, 2);
	insert into otusdb.customers_building_numbers (customer_id, building_number_id) values (2, 3);
	insert into otusdb.customers_building_numbers (customer_id, building_number_id) values (3, 4);
	insert into otusdb.customers_building_numbers (customer_id, building_number_id) values (4, 5);
	insert into otusdb.customers_building_numbers (customer_id, building_number_id) values (5, 6);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_category_products;
delimiter //
create procedure otusdb.insert_data_category_products()
BEGIN
	insert into otusdb.category_products (name, characteristic) values ('Характеристики 1 red', '{"color": "red", "width": "1000", "heigth": "1200"}');
	insert into otusdb.category_products (name, characteristic) values ('Характеристики 2 blue', '{"color": "blue", "width": "1100", "heigth": "1300"}');
	insert into otusdb.category_products (name, characteristic) values ('Характеристики 3 green', '{"color": "green", "width": "1200", "heigth": "1400"}');
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_organisations;
delimiter //
create procedure otusdb.insert_data_organisations()
BEGIN
	insert into otusdb.organizations (name, fullname, INN, KPP, id_building_number, type_organisation) values ('Рога', 'ООО "Рога"', '6546545656566', '87479878798', 7, 0);
	insert into otusdb.organizations (name, fullname, INN, KPP, id_building_number, type_organisation) values ('Копыта', 'ООО "Копыта"', '98798213213', '89789123214', 8, 1);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_products;
delimiter //
create procedure otusdb.insert_data_products()
BEGIN
	insert into otusdb.products (name, id_category_procuct, id_manufacturer, id_provider) values ('Продукт 1', 1, 1, 2);
	insert into otusdb.products (name, id_category_procuct, id_manufacturer, id_provider) values ('Продукт 2', 2, 1, 2);
	insert into otusdb.products (name, id_category_procuct, id_manufacturer, id_provider) values ('Продукт 3', 3, 1, 2);
	insert into otusdb.products (name, id_category_procuct, id_manufacturer, id_provider) values ('Продукт 4', 1, 1, 1);
	insert into otusdb.products (name, id_category_procuct, id_manufacturer, id_provider) values ('Продукт 5', null, 1, 2);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_prices;
delimiter //
create procedure otusdb.insert_data_prices()
BEGIN
	insert into otusdb.prices (product_id, cost) values (1, 100.00);
	insert into otusdb.prices (product_id, cost) values (2, 2200.23);
	insert into otusdb.prices (product_id, cost) values (3, 100.50);
	insert into otusdb.prices (product_id, cost) values (4, 11.00);
	insert into otusdb.prices (product_id, cost) values (5, 78.00);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_history_prices;
delimiter //
create procedure otusdb.insert_data_history_prices()
BEGIN
	insert into otusdb.history_prices (product_id, date_from, date_to, cost) values (1, '2019-11-01', '2019-11-30', 90.00);
	insert into otusdb.history_prices (product_id, date_from, date_to, cost) values (2, '2019-11-01', '2019-11-30', 2000.00);
	insert into otusdb.history_prices (product_id, date_from, date_to, cost) values (3, '2019-11-01', '2019-11-30', 89.00);
	insert into otusdb.history_prices (product_id, date_from, date_to, cost) values (4, '2019-10-01', '2019-10-31', 12.00);
	insert into otusdb.history_prices (product_id, date_from, date_to, cost) values (4, '2019-11-01', '2019-11-30', 9.00);
	insert into otusdb.history_prices (product_id, date_from, date_to, cost) values (5, '2019-11-01', '2019-11-30', 82.00);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_type_payments;
delimiter //
create procedure otusdb.insert_data_type_payments()
BEGIN
	insert into otusdb.type_payments (type_name) values ('Наличными');
	insert into otusdb.type_payments (type_name) values ('Картой онлайн');
	insert into otusdb.type_payments (type_name) values ('Банковкий перевод');
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_order_statuses;
delimiter //
create procedure otusdb.insert_data_order_statuses()
BEGIN
	insert into otusdb.order_statuses (name) values ('Оформлен');
	insert into otusdb.order_statuses (name) values ('В работе');
	insert into otusdb.order_statuses (name) values ('Заказан');
	insert into otusdb.order_statuses (name) values ('Готов к отправке');
	insert into otusdb.order_statuses (name) values ('Отправлен');
	insert into otusdb.order_statuses (name) values ('Доставлен');
	insert into otusdb.order_statuses (name) values ('Выполнен');
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_history_order_statuses;
delimiter //
create procedure otusdb.insert_data_history_order_statuses()
BEGIN
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-01', 1, 1);
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-01', 1, 2);
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-01', 1, 3);
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-01', 1, 4);	
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-02', 1, 5);	
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-05', 1, 6);	
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-06', 1, 7);
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-03', 2, 1);
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-03', 2, 2);
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-04', 2, 3);
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-05', 2, 4);	
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-05', 2, 5);	
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-11', 2, 6);	
	insert into otusdb.history_order_statuses (date_status, id_order, id_status) values ('2019-12-16', 2, 7);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_orders;
delimiter //
create procedure otusdb.insert_data_orders()
BEGIN
	insert into otusdb.orders (date_order, date_receiving, id_type_payment, id_customer, total, bonus) values ('2019-12-01', '2019-12-06', 2, 1, 111.00, 6);
	insert into otusdb.orders (date_order, date_receiving, id_type_payment, id_customer, total, bonus) values ('2019-12-03', '2019-12-10', 3, 3, 2278.23, 228);
END
//
delimiter ;

DROP PROCEDURE IF EXISTS otusdb.insert_data_purchases;
delimiter //
create procedure otusdb.insert_data_purchases()
BEGIN
	insert into otusdb.purchases (id_product, count, id_order, bonus) values (1, 1, 1, 5);
	insert into otusdb.purchases (id_product, count, id_order, bonus) values (4, 1, 1, 1);
	insert into otusdb.purchases (id_product, count, id_order, bonus) values (2, 1, 2, 220);
	insert into otusdb.purchases (id_product, count, id_order, bonus) values (5, 1, 2, 8);
END
//
delimiter ;


start transaction;
	CALL otusdb.insert_data_correspondence_languages();
commit;

start transaction;
	CALL otusdb.insert_data_titles();
commit;

start transaction;
	CALL otusdb.insert_data_marital_statuses();
commit;

start transaction;
	CALL otusdb.insert_data_customer_statuses();
commit;

start transaction;
	CALL otusdb.insert_data_countries();
commit;

start transaction;
	CALL otusdb.insert_data_regions();
commit;

start transaction;
	 CALL otusdb.insert_data_cities();
commit;

start transaction;
	CALL otusdb.insert_data_streets();
commit;

start transaction;
	CALL otusdb.insert_data_building_numbers();
commit;

start transaction;
	CALL otusdb.insert_data_customers();
commit;

start transaction;
	CALL otusdb.insert_data_customers_building_numbers();
commit;

start transaction;
	CALL otusdb.insert_data_category_products();
commit;

start transaction;
	CALL otusdb.insert_data_organisations();
commit;

start transaction;
	CALL otusdb.insert_data_type_payments();
commit;

start transaction;
	CALL otusdb.insert_data_order_statuses();
commit;

start transaction;
	CALL otusdb.insert_data_products();
commit;

start transaction;
	CALL otusdb.insert_data_prices();
commit;

start transaction;
	CALL otusdb.insert_data_history_prices();
commit;

start transaction;
	CALL otusdb.insert_data_orders();
commit;

start transaction;
	CALL otusdb.insert_data_history_order_statuses();
commit;

start transaction;
	CALL otusdb.insert_data_purchases();
commit;