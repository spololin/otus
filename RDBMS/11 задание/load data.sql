--LOAD DATA

LOAD DATA INFILE '/var/lib/mysql-files/Apparel.csv'
	 INTO TABLE apparel
	 FIELDS TERMINATED BY ','
	 ENCLOSED BY '"'
 	 LINES TERMINATED BY '\n'
 IGNORE 1 ROWS;
 
 
 LOAD DATA INFILE '/var/lib/mysql-files/Bicycles.csv'
	 INTO TABLE bicycles
	 FIELDS TERMINATED BY ','
	 ENCLOSED BY '"'
 	 LINES TERMINATED BY '\n'
 IGNORE 1 ROWS;
 
  LOAD DATA INFILE '/var/lib/mysql-files/Fashion.csv'
	 INTO TABLE fashion
	 FIELDS TERMINATED BY ','
	 ENCLOSED BY '"'
 	 LINES TERMINATED BY '\n'
 IGNORE 1 ROWS;
 
   LOAD DATA INFILE '/var/lib/mysql-files/jewelry.csv'
	 INTO TABLE jewelry
	 FIELDS TERMINATED BY ','
	 ENCLOSED BY '"'
 	 LINES TERMINATED BY '\n'
 IGNORE 1 ROWS;
 
 
    LOAD DATA INFILE '/var/lib/mysql-files/SnowDevil.csv'
	 INTO TABLE snowdevil
	 FIELDS TERMINATED BY ','
	 ENCLOSED BY '"'
 	 LINES TERMINATED BY '\n'
 IGNORE 1 ROWS;
 
--mysqlimport
mysqlimport apparel --ignore-lines=1 --lines-terminated-by="\n" --fields-terminated-by="," --fields-enclosed-by="\""  "./Apparel.csv"
mysqlimport bicycles --ignore-lines=1 --lines-terminated-by="\n" --fields-terminated-by="," --fields-enclosed-by="\""  "./Bicycles.csv"
mysqlimport fashion --ignore-lines=1 --lines-terminated-by="\n" --fields-terminated-by="," --fields-enclosed-by="\""  "./Fashion.csv"
mysqlimport jewelry --ignore-lines=1 --lines-terminated-by="\n" --fields-terminated-by="," --fields-enclosed-by="\""  "./jewelry.csv"
mysqlimport snowdevil --ignore-lines=1 --lines-terminated-by="\n" --fields-terminated-by="," --fields-enclosed-by="\""  "./SnowDevil.csv" 
 
 
 
 
 