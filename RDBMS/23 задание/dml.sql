--1. Регулярные выражения
--найти всех организации c формой регистрации ООО или ОАО
select * from sales.organizations o 
where o.fullname similar to '(ООО|ОАО)%'='t'


--2. Запрос с inner join. При смене порядка соединений сначала данные будут выбираться из второй таблицы, и пристыковываться к первой.
-- получить список всех существующих продуктов, купленных за все время.
select p.*, pr.id from products.products p 
join sales.purchases as pr on pr.id_product = p.id

-- запрос с left join. Получить список всех существующих продуктов c характеристиками.
select p.*, cat.characteristic from products.products p
left join products.category_products as cat on cat.id = p.id_category_product


-- 3. Запрос update from. Установить поле phone в значение null для всех покупателей, которые живут на улице Пролетарская города с id=5
UPDATE customers.customers SET phone = null FROM customers.building_numbers as bns
  WHERE bns.id_street = (select id from customers.streets where "name" = 'Пролетарская' and id_city=5 limit 1)
  

--4. delete c join. Удалить всех покупателей, которые сделали не пустые заказы
DELETE FROM customers.customers as c
USING sales.orders as o
join sales.purchases as p on o.id = p.id_order
WHERE c.id =o.id_customer;

--5. copy
create table countries2 (id serial NOT NULL,
	"name" varchar(200) NULL,
	code varchar(5) NOT NULL
);

COPY customers.countries TO 'C:\data.csv'; 
COPY customers.countries2 FROM 'C:\data.csv'; 


