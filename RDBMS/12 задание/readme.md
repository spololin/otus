CASE. Выбрать все организации с определением типа организации

	CREATE OR REPLACE VIEW otusdb.case_select AS
	SELECT 
		name, 
		inn, 
		kpp,
		CASE 
			WHEN type_organisation=0 THEN 'Производитель'
			ELSE 'Поставщик'
			END AS 'Тип организации'
	FROM otusdb.organizations;
	
	
HAVING. Выбрать все языки, которые выбраны основными для нескольких покупателей или сотрудников, у которых основной язык украинский.

	CREATE OR REPLACE VIEW otusdb.having_select AS
	SELECT 
		cls.code, 
		cls.description,
		COUNT(cts.id)
	FROM otusdb.customers AS cts
	LEFT JOIN otusdb.correspondence_languages AS cls ON cls.id=cts.id_correspondence_language
	GROUP BY cls.code
	HAVING COUNT(cts.id)>1 OR cls.code='UA';
	
ROLLUP. Выбрать количество товаров по категориям

	SELECT 
		IF (GROUPING (cat.name) = 1, 'Итого',
			   cat.name) AS category_name, 
		COUNT(prd.id),
        (SELECT MIN(cost) FROM prices AS prs where prs.product_id IN (
		SELECT id FROM products WHERE id_category_product=cat.id
		)) AS min,
        (SELECT MAX(cost) FROM prices AS prs where prs.product_id IN (
		SELECT id FROM products WHERE id_category_product=cat.id
		)) AS max
	FROM products AS prd
	LEFT JOIN category_products AS cat ON cat.id=prd.id_category_product
	GROUP BY cat.name
	WITH ROLLUP;
	
Выборать самый дорогой и самый дешевый товар в каждой категории

	CREATE OR REPLACE VIEW otusdb.min_max_select AS
		with tb as (
select 
	cat.id,
	cat.name,
	(
		select 
			max(cost) as max_cost 
		from prices 
		where product_id in (select id from products where id_category_product=cat.id)
	) as max,
	(
		select 
			min(cost) as min_cost 
		from prices 
		where product_id in (select id from products where id_category_product=cat.id)
	) as min
	from category_products as cat) 
	select 
		tb.name, 
		(select 
			name 
		from products as prd 
		left join prices as prs on prd.id=prs.product_id
		where prd.id_category_product=tb.id and prs.cost=tb.min) as min,
		(select 
			name 
		from products as prd 
		left join prices as prs on prd.id=prs.product_id
		where prd.id_category_product=tb.id and prs.cost=tb.max) as max
	from tb