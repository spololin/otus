В таблицу category_products вместо полей color, width, height добавил поле characteristic, которое будет схранить характеристики продуктов.
Пример вставки:
INSERT INTO category_products VALUES('{"color": "red", "width": "1000", "heigth": "1200"}');
Пример выборки: 
SELECT * FROM category_products WHERE JSON_EXTRACT(`attributes` , '$.color') = 'red' AND JSON_EXTRACT(`attributes` , '$.width') > 900;


Партиции
Для таблицы customers портиции по полю birth_date для аналитики покупателей по возрасту
Для таблицы orders портиции по полю date_order для выделения прошлогодних заказов и старше отдельно