Выбрать все купленные продукты:
SELECT pd.*, pr.* FROM products AS pd
JOIN purchases AS pr ON pr.id_product = pd.id

Выбрать все покупки с информацией о заказе:
SELECT pr.*, ord.* FROM purchases AS pr
LEFT JOIN orders AS ord ON pr.id_order = ord.id

Выбрать для заказы этого года (для отчета пуководству): 
SELECT * FROM orders 
WHERE YAER(date_order)=YEAR(NOW())

//---------------------------------------------------
Выбрать все товары, купленные покупателем с ID=777
SELECT pr.*, prc.count FROM products AS pr
LEFT JOIN purchases AS prc on prc.id_products = pr.id
JOIN orders AS ord on (ord.id_purchases = prc.id and ord.id_customer = 777)

Выбрать продукты, у которых актуальная стоимость выше 1000 руб (фильтр на сайте):
SELECT * FROM products AS pr
LEFT JOIN prices AS pri on (pri.product_id=pr.id and pr.cost>1000)

Выбрать все неоплаченные заказы (для отчета)
SELECT * FROM orders WHERE id_type_payment IS NULL

Выбрать все категории продуктов, у которых цвет красный и ширина больше 900см (фильтр на сайте)
SELECT * FROM category_products 
WHERE JSON_EXTRACT(`attributes` , '$.color') = 'red' 
	AND JSON_EXTRACT(`attributes` , '$.width') > 900;
	
Посчитать сумму стоимостей всех заказаов, сделанных с 01.01.2019 по 08.01.2019 (для отчета руководству)
SELECT SUM(total) FROM orders
where date_order BETWEEN '01.01.2019' AND '08.01.2019'


